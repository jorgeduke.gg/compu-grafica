var vertexShaderText = 
[
'precision mediump float;',
'',
'attribute vec2 vertPosition;',
'attribute vec3 vertColor;',
'varying vec3 fragColor;',
'',
'void main()',
'{',
'  fragColor = vertColor;',
'  gl_Position = vec4(vertPosition, 0.0, 1.0);',
'}'
].join('\n');

var fragmentShaderText =
[
'precision mediump float;',
'',
'varying vec3 fragColor;',
'void main()',
'{',
'  gl_FragColor = vec4(fragColor, 1.0);',
'}'
].join('\n');

var InitDemo = function () {
	console.log('This is working');

	var canvas = document.getElementById('game-surface');
	var gl = canvas.getContext('webgl');

	if (!gl) {
		console.log('WebGL not supported, falling back on experimental-webgl');
		gl = canvas.getContext('experimental-webgl');
	}

	if (!gl) {
		alert('Your browser does not support WebGL');
	}

	gl.clearColor(0.97, 0.97, 2.8, 2.0);
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	//
	// Create shaders
	// 
	var vertexShader = gl.createShader(gl.VERTEX_SHADER);
	var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

	gl.shaderSource(vertexShader, vertexShaderText);
	gl.shaderSource(fragmentShader, fragmentShaderText);

	gl.compileShader(vertexShader);
	if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
		console.error('ERROR compiling vertex shader!', gl.getShaderInfoLog(vertexShader));
		return;
	}

	gl.compileShader(fragmentShader);
	if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
		console.error('ERROR compiling fragment shader!', gl.getShaderInfoLog(fragmentShader));
		return;
	}

	var program = gl.createProgram();
	gl.attachShader(program, vertexShader);
	gl.attachShader(program, fragmentShader);
	gl.linkProgram(program);
	if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
		console.error('ERROR linking program!', gl.getProgramInfoLog(program));
		return;
	}
	gl.validateProgram(program);
	if (!gl.getProgramParameter(program, gl.VALIDATE_STATUS)) {
		console.error('ERROR validating program!', gl.getProgramInfoLog(program));
		return;
	}

	//
	// Create buffer
	//
	var triangleVertices = 
	[ // X, Y,       R, G, B
		0 ,0,   	1.0, 1.0,0.0,
		0.3,0,  0.7, 0.0,1.0,
		0,0.9,   0.1, 1.0,0.6
	];

	var triangleVertexBufferObject = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, triangleVertexBufferObject);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVertices), gl.STATIC_DRAW);

	var positionAttribLocation = gl.getAttribLocation(program, 'vertPosition');
	var colorAttribLocation = gl.getAttribLocation(program, 'vertColor');
	gl.vertexAttribPointer(
		positionAttribLocation, // Attribute location
		2, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		5 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		0 // Offset from the beginning of a single vertex to this attribute
	);
	gl.vertexAttribPointer(
		colorAttribLocation, // Attribute location
		3, // Number of elements per attribute
		gl.FLOAT, // Type of elements
		gl.FALSE,
		5 * Float32Array.BYTES_PER_ELEMENT, // Size of an individual vertex
		2 * Float32Array.BYTES_PER_ELEMENT // Offset from the beginning of a single vertex to this attribute
	);

	gl.enableVertexAttribArray(positionAttribLocation);
	gl.enableVertexAttribArray(colorAttribLocation);

	//
	// Main render loop
	//
	gl.useProgram(program);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	
	//piso1
	var triangleVerticespiso1 = 
	[ // X, Y,       R, G, B
		-1, -.25,    56/255,149/255,73/255,
		-1, -1,   56/255,149/255,73/255,
		1, -.25,    56/255,149/255,73/255,
	];

	var triangleVertex2BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespiso1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	
	//piso2
	var triangleVerticespiso2 = 
	[ // X, Y,       R, G, B
		1, -0.25,     56/255,149/255,73/255,
		1, -1,    56/255,149/255,73/255,
		-1, -1,    56/255,149/255,73/255,
	];
    var triangleVertex2BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespiso2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
   //pared1
	var triangleVerticesp1 = 
	[ // X, Y,       R, G, B
		-1, -.25,    20/255,87/255,127/255,
		-1, 1,  20/255,87/255,127/255,
		1, 1,   20/255,87/255,127/255,
	];

	var triangleVertex2BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticesp1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	
	//pared2
	var triangleVerticespa2 = 
	[ // X, Y,       R, G, B
		1, 1,    20/255,87/255,127/255,
		1, -0.25,   20/255,87/255,127/255,
		-1, -0.25,   20/255,87/255,127/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespa2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    //puerta1
	var triangleVerticespu2 = 
	[ // X, Y,       R, G, B
		1, -0.25,    241/255,167/255,67/255,
		0.8,-0.25,   241/255,167/255,67/255,
		0.8, 0.75,   241/255,167/255,67/255,
	];
    	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespu2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    //puerta2
    var triangleVerticespu1 = 
	[ // X, Y,       R, G, B
		0.8, 0.75,    241/255,167/255,67/255,
		1 , 0.75,   241/255,167/255,67/255,
		1, -0.25,   241/255,167/255,67/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespu1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    //picap
    var triangleVerticespi = 
	[ // X, Y,       R, G, B
		0.84, 0.2,    245/255,248/255,38/255,
		0.83 , 0.25,    245/255,248/255,38/255,
		0.82, 0.2,    245/255,248/255,38/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespi), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    //cama1
    var triangleVerticescama1 = 
	[ // X, Y,       R, G, B
		-0.25, -0.25,    149/255,125/255,56/255,
		0.25 , -0.25,    149/255,125/255,56/255,
		0.25, 0,    149/255,125/255,56/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescama1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    //cama2
    var triangleVerticescama2 = 
	[ // X, Y,       R, G, B
		-0.25, -0.25,    149/255,125/255,56/255,
		-0.25 , 0,    149/255,125/255,56/255,
		0.25, 0,    149/255,125/255,56/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescama2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
     //patas1
    var triangleVerticespat1 = 
	[ // X, Y,       R, G, B
		-0.25, -0.25,    149/255,125/255,56/255,
		-0.15 , -0.25,    149/255,125/255,56/255,
		-0.15, -0.35,    149/255,125/255,56/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespat1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    //patas2
    var triangleVerticespat2 = 
	[ // X, Y,       R, G, B
		-0.25, -0.25,    149/255,125/255,56/255,
		-0.25 , -0.35,    149/255,125/255,56/255,
		-0.15, -0.35,    149/255,125/255,56/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespat2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    //patas3
    var triangleVerticespat3 = 
	[ // X, Y,       R, G, B
		0.25, -0.25,    149/255,125/255,56/255,
		0.15 , -0.25,    149/255,125/255,56/255,
		0.15, -0.35,    149/255,125/255,56/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespat3), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    //patas4
    var triangleVerticespat4 = 
	[ // X, Y,       R, G, B
		0.25, -0.25,    149/255,125/255,56/255,
		0.25 , -0.35,    149/255,125/255,56/255,
		0.15, -0.35,    149/255,125/255,56/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespat4), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
     //patas5
    var triangleVerticespat5 = 
	[ // X, Y,       R, G, B
		-0.15, -0.25,    109/255,93/255,44/255,
		-0.10 , -0.25,    109/255,93/255,44/255,
		-0.15, -0.30,    109/255,93/255,44/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespat5), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    //patas6
    var triangleVerticespat6 = 
	[ // X, Y,       R, G, B
		-0.10, -0.25,    109/255,93/255,44/255,
		-0.15 , -0.3,    109/255,93/255,44/255,
		-0.10, -0.3,    109/255,93/255,44/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespat6), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
     //patas7
    var triangleVerticespat7 = 
	[ // X, Y,       R, G, B
		0.15, -0.25,    109/255,93/255,44/255,
		0.10 , -0.25,    109/255,93/255,44/255,
		0.15, -0.30,    109/255,93/255,44/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespat7), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    //patas8
    var triangleVerticespat8 = 
	[ // X, Y,       R, G, B
		0.10, -0.25,    109/255,93/255,44/255,
		0.15 , -0.3,    109/255,93/255,44/255,
		0.10, -0.3,    109/255,93/255,44/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespat8), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	 //pies homero 1
	    var triangleVerticeshhp1 = 
	[ // X, Y,       R, G, B
		-0.9, -0.75,    196/255,172/255,84/255,
		-0.9 , -0.9,    196/255,172/255,84/255,
		-0.75, -0.9,    196/255,172/255,84/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticeshhp1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	 //pies homero 2
	    var triangleVerticeshhp2 = 
	[ // X, Y,       R, G, B
		-0.75, -0.9,    196/255,172/255,84/255,
		-0.75 , -0.75,    196/255,172/255,84/255,
		-0.9, -0.75,  196/255,172/255,84/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticeshhp2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	//piernas homero 1
	    var triangleVerticespp1 = 
	[ // X, Y,       R, G, B
		-0.9, -0.74,    93/255,171/255,239/255,
		-0.93 , -0.29,   93/255,171/255,239/255,
		-0.80, -0.74,   93/255,171/255,239/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespp1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	 //piernas homero 2
	    var triangleVerticespp2 = 
	[ // X, Y,       R, G, B
		-0.93, -0.3,   93/255,171/255,239/255,
		-0.76 , -0.3,   93/255,171/255,239/255,
		-0.80, -0.74,    93/255,171/255,239/255,
	];
		var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespp2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	//cuerpo homero 1
	    var triangleVerticescp1 = 
	[ // X, Y,       R, G, B
		-0.93, -0.29,    230/255,235/255,237/255,
		-0.93 , 0.3,    230/255,235/255,237/255,
		-0.65, -0.29,   230/255,235/255,237/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescp1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	 // cuerpo homero 2
	    var triangleVerticescp2 = 
	[ // X, Y,       R, G, B
		-0.93, 0.3,   230/255,235/255,237/255,
		-0.7 , 0.1,    230/255,235/255,237/255,
		-0.65, -0.29,   230/255,235/255,237/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescp2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	 // cuerpo homero 3
	    var triangleVerticescp3 = 
	[ // X, Y,       R, G, B
		-0.93, 0.3,    230/255,235/255,237/255,
		-0.7 , 0.3,    230/255,235/255,237/255,
		-0.65, -0.29,   230/255,235/255,237/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescp3), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	
	//brazo homero 1
	    var triangleVerticesbraz1 = 
	[ // X, Y,       R, G, B
		-0.85, 0.2,    249/255,249/255,34/255,
		-0.85 , -0.1,    249/255,249/255,34/255,
		-0.78, -0.1,   249/255,249/255,34/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticesbraz1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	 // brazo homero 2
	    var triangleVerticesbraz2 = 
	[ // X, Y,       R, G, B
		-0.85, 0.2,   249/255,249/255,34/255,
		-0.78 , -0.1,   249/255,249/255,34/255,
		-0.85, 0.2,  249/255,249/255,34/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticesbraz2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
		//cuello homero 1
	    var triangleVerticesccc1 = 
	[ // X, Y,       R, G, B
		-0.8, 0.3,   230/255,235/255,237/255,
		-0.85 , 0.3,    230/255,235/255,237/255,
		-0.85, 0.4,    230/255,235/255,237/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticesccc1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	 // cuello homero 2
	    var triangleVerticesccc2 = 
	[ // X, Y,       R, G, B
		-0.85, 0.4,    249/255,249/255,34/255,
		-0.8 , 0.4,     249/255,249/255,34/255,
		-0.8, 0.3,      249/255,249/255,34/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticesccc2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	
	//cabeza homero1
	    var triangleVerticescabez1 = 
	[ // X, Y,       R, G, B
		-0.77, 0.7,     249/255,249/255,34/255,
		-0.87 , 0.4,    249/255,249/255,34/255,
		-0.9, 0.7,     249/255,249/255,34/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescabez1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	 // cabeza homero2 
	    var triangleVerticescabez2 = 
	[ // X, Y,       R, G, B
	   -0.87, 0.4,    249/255,249/255,34/255,
		-0.77 , 0.4,    249/255,249/255,34/255,
		-0.77, 0.7,    249/255,249/255,34/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescabez2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	//barba homero1
	    var triangleVerticesbarb1 = 
	[ // X, Y,       R, G, B
		-0.77, 0.4,    196/255,172/255,84/255,
		-0.83 , 0.5,    196/255,172/255,84/255,
		-0.83, 0.4,    196/255,172/255,84/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticesbarb1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	 // barba homero2 
	    var triangleVerticesbarb2 = 
	[ // X, Y,       R, G, B
	   -0.83, 0.5,   196/255,172/255,84/255,
		-0.77 , 0.5,   196/255,172/255,84/255,
		-0.77, 0.4,    196/255,172/255,84/255,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticesbarb2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	//ojos homero1
	    var triangleVerticesojos1 = 
	[ // X, Y,       R, G, B
		-0.78, 0.55,    1,1,1,
		-0.78 , 0.65,   1,1,1,
		-0.83 , 0.65,   1,1,1,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticesojos1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	 // ojos homero2 
	    var triangleVerticesojos2 = 
	[ // X, Y,       R, G, B
	   -0.83, 0.65,   1,1,1,
		-0.83 , 0.55,   1,1,1,
		-0.78, 0.55,    1,1,1,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticesojos2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	//pupi homero1
	    var triangleVerticespupi1 = 
	[ // X, Y,       R, G, B
		-0.79, 0.56,   0,0,0,
		-0.79 , 0.58,   0,0,0,
		-0.80 , 0.58,   0,0,0,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespupi1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	 // pupi2 homero2 
	    var triangleVerticespupi2 = 
	[ // X, Y,       R, G, B
	   -0.80, 0.58,   0,0,0,
		-0.80 , 0.56,   0,0,0,
		-0.79, 0.56,    0,0,0,
	];
	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticespupi2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	// CAJON 1
    var triangleVerticescajon = 
	[ // X, Y,       R, G, B
		0.3, -0.3,    109/255,93/255,44/255,
		0.5 , 0,    109/255,93/255,44/255,
		0.5, -0.3,    109/255,93/255,44/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescajon), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    // CAJON 2
    var triangleVerticescajon2 = 
	[ // X, Y,       R, G, B
		0.3, 0,    109/255,93/255,44/255,
		0.5 , 0,    109/255,93/255,44/255,
		0.3, -0.3,    109/255,93/255,44/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescajon2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	// CAJON 3
    var triangleVerticescajon3 = 
	[ // X, Y,       R, G, B
		-0.3, -0.3,    109/255,93/255,44/255,
		-0.5 , 0,    109/255,93/255,44/255,
		-0.5, -0.3,    109/255,93/255,44/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescajon3), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    // CAJON 4
    var triangleVerticescajon4 = 
	[ // X, Y,       R, G, B
		-0.3, 0,    109/255,93/255,44/255,
		-0.5 , 0,    109/255,93/255,44/255,
		-0.3, -0.3,    109/255,93/255,44/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescajon4), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
	// CUADRO1
    var triangleVerticescuadro1 = 
	[ // X, Y,       R, G, B
		-0.3, 0.4,    1,1,1,
		0.3 , 0.4,     1,1,1,
		0.3, 0.8,    1,1,1,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescuadro1), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    // CUADRO2
    var triangleVerticescuadro2 = 
	[ // X, Y,       R, G, B
		-0.3, 0.4,    1,1,1,
		-0.3 , 0.8,    1,1,1,
		0.3, 0.8,     1,1,1,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticescuadro2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    //cabecera cama 1
    var triangleVerticesca = 
	[ // X, Y,       R, G, B
		0.2, 0.3,    109/255,93/255,44/255,
		-0.2 , 0.3,    109/255,93/255,44/255,
		-0.2, 0,    109/255,93/255,44/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticesca), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
    //cabecera cama2
    var triangleVerticesca2 = 
	[ // X, Y,       R, G, B
		0.2, 0.3,    109/255,93/255,44/255,
		0.2 , 0,    109/255,93/255,44/255,
		-0.2, -0,    109/255,93/255,44/255,
	];

	var triangleVertex3BufferObject = gl.createBuffer();
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(triangleVerticesca2), gl.STATIC_DRAW);
	gl.drawArrays(gl.TRIANGLES, 0, 3);
};
